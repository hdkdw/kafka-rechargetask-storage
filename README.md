# Kafka-rechargeTaskRequest


Aplikasi untuk mengirimkan data RechargeTask menggunakan Apache Kafka.
Terdiri dari :
1. Aplikasi Publisher RechargeTaskRequest --- [link Gitlab](#https://gitlab.com/hdkdw/kafka-rechargetaskrequest)
2. Aplikasi Consumer RechargeTask-Storage --- [link Gitlab](https://gitlab.com/hdkdw/kafka-rechargetask-storage)


## Cara Menggunakan
1. Running Kafka dengan docker-compose --- [link Gitlab](https://gitlab.com/hdkdw/kafka-with-docker)  
Setting Konfigurasi Kafka sesuai yang ada di dalam application.yml di masing-masing aplikasi (1 zookeeper dengan 3 broker).  
Pastikan semua container broker dan zookeeper up dan running.  
2. Buat topic di Kafka dengan nama : t.recharge.task
3. Running aplikasi publisher RechargeTaskRequest
    - Aplikasi ini kafka producer untuk mengirimkan data RechargeTask Request ke kafka, dimana data request tersebut di submit melalui API
    - POST data RechargeTask di link api : localhost:8080/api/rechargetask/submit dengan contoh format sbb 
    ```

    {
        "scheduleId" : "aaaa",
        "amount" : 100
    }

    ```
    


4. Running aplikasi consumer RechargeTask-Storage
    - Aplikasi RechargeTask-Storage adalah Kafka consumer, dimana akan menerima message dari kafka, kemudian akan disimpan di dalam database
    - pada aplikasi ini database disimulasikan dengan H2
    - Jika sudah di running dan data RechargeTaskRequest sudah di submit, check data yang  berhasil disimpan di link : http://localhost:8081/h2-console
    - pastikan field jdbc url disamakan dengan setting yg ada di application.yml (jdbc:h2:mem:testdb) 


