package com.project.kafka.service;

import com.project.kafka.dto.RechargeTaskRequest;
import com.project.kafka.entity.RechargeTask;
import com.project.kafka.repo.RechargeTaskStorageRepository;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RechargeTaskStorageService {

    @Autowired
    private RechargeTaskStorageRepository rechargeTaskStorageRepository;

    @Autowired
    private ModelMapper modelMapper;

    public static final Logger logger = LoggerFactory.getLogger(RechargeTaskStorageService.class);

    public void save(RechargeTaskRequest request){

        logger.info("data to be saved : {} ", request);

       RechargeTask rechargeTask = new RechargeTask();
        rechargeTask.setScheduleId(request.getScheduleId());
        rechargeTask.setAmount(request.getAmount());

        rechargeTaskStorageRepository.save(rechargeTask);
    }


}
