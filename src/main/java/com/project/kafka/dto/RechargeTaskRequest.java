package com.project.kafka.dto;

public class RechargeTaskRequest {

    private String scheduleId;

    private Integer amount;

    public RechargeTaskRequest(){

    }

    public RechargeTaskRequest(String scheduleId, Integer amount) {
        this.scheduleId = scheduleId;
        this.amount = amount;
    }

    public String getScheduleId() {
        return scheduleId;
    }

    public void setScheduleId(String scheduleId) {
        this.scheduleId = scheduleId;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }
}
