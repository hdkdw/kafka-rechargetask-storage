package com.project.kafka.utility;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.project.kafka.dto.RechargeTaskRequest;
import org.apache.kafka.common.serialization.Deserializer;
import java.util.Map;

public class PayloadDeserializer implements Deserializer <RechargeTaskRequest>{

    @Override
    public void close() {
    }
    @Override
    public void configure(Map<String, ?> arg0, boolean arg1) {
    }
    @Override
    public RechargeTaskRequest deserialize(String arg0, byte[] arg1) {
        ObjectMapper mapper = new ObjectMapper();
        RechargeTaskRequest request = null;
        try {
            request = mapper.readValue(arg1, RechargeTaskRequest.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return request;
    }

}


