package com.project.kafka.repo;

import com.project.kafka.entity.RechargeTask;
import org.springframework.data.repository.CrudRepository;

public interface RechargeTaskStorageRepository extends CrudRepository<RechargeTask, Integer> {
}
