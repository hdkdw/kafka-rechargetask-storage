package com.project.kafka.consumer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.project.kafka.dto.RechargeTaskRequest;
import com.project.kafka.service.RechargeTaskStorageService;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
public class RechargeTaskConsumer {

    private static final Logger logger = LoggerFactory.getLogger(RechargeTaskConsumer.class);

    @Autowired
    private ObjectMapper objectMapper = new ObjectMapper();

    @Autowired
    private RechargeTaskStorageService rechargeTaskStorageService;

    @KafkaListener(topics = "t.recharge.task")
    public void consumeRechargeTaskRequest(ConsumerRecord<String, String> consumerRecord) throws JsonProcessingException {

        RechargeTaskRequest request = objectMapper.readValue(consumerRecord.value(), RechargeTaskRequest.class);

        logger.info("request @ consumer : {} ", request);
        rechargeTaskStorageService.save(request);

    }

}
